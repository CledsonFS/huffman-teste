#include <iostream>
#include <fstream>
#include "item1e2.h"

using namespace std;

Arquivo::Arquivo()
{
    temp = NULL;
    tam = 0;
}
Arquivo::~Arquivo()
{
    delete[] temp;
    arq.close();
    copia.close();
}
void Arquivo::abrir_arq() // abrir arquivo
{
    cout << "Local e nome do arquivo: ";
    cin.getline(nome, 50);

    arq.open(nome, ios::binary);
    if(!arq.is_open())
        cout << endl << "Erro ao abrir arquivo: "
             << nome << endl << endl;
    else
        cout << endl << "Arquivo: " << nome
             << " aberto com sucesso!" << endl;
}
void Arquivo::tam_arq() // tamanho do arquivo
{
   arq.seekg(0, arq.end);
   tam = arq.tellg();
   arq.seekg(0, arq.beg);
   cout << "Tamanho: " << tam
        << " Bytes" << endl << endl;
}
void Arquivo::ler_arq() // ler arquivo
{
    temp = new char[tam];
    arq.read(temp, tam);
    if(arq.fail())
    {
        cout << "Erro ao ler arquivo!" << endl << endl;
        arq.close();
    }
    else
        for (int i = 0; i < tam; i++)
            cout << temp[i];
    cout << endl;
}
void Arquivo::copia_arq()
{
// copia do arquivo binario p/ comparar com original no HEX EDITOR
    copia.open("copiaTeste.txt", ios::binary);
    copia.write(temp, tam);
    if(!copia.fail())
        cout << endl << "Copia realizada com sucesso!"
             << endl << endl;
    else
        cout << "Copia falhou!" << endl << endl;
}
void Arquivo::cont_bytes() // vetor de ocorrência de bytes
{
    int i, j, k, cont = 1;
    int aux[tam];

    for(i = 0; i < tam; i++)
        aux[i] = temp[i];
    for(i = 0; i < tam; i++)
    {
        for(j = i+1; j < tam; j++)
        {
            if(aux[i] == aux[j])
            {
                for(k = j; k < tam; k++)
                    aux[k] = aux[k+1];
                tam--;
                cont++;
            }
        }
        cout << "Caractere: " << hex << aux[i] // testar a ocorrência
             << "\tAparece: " << cont
             << endl;
        cont = 1;
    }
    cout << endl << endl;
}
