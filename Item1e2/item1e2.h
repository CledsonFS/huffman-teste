#ifndef ITEM1E2
#define ITEM1E2
#include <iostream>
#include <fstream>

using namespace std;

class Arquivo
{
private:
    ifstream arq;
    ofstream copia;
    char nome[50];
    long long tam;
    char *temp;
public:
    Arquivo();
    ~Arquivo();
    void abrir_arq();   // abrir arquivo
    void tam_arq();     // tamanho do arquivo
    void ler_arq();     // ler arquivo
    void copia_arq();   // copia do arquivo binario
    void cont_bytes();  // contar repetiçao de Bytes
};

#endif // ITEM1E2

