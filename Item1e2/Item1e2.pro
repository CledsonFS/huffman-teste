TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    item1e2.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    item1e2.h

