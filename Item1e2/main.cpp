#include <iostream>
#include <fstream>
#include "item1e2.h"

int main()
{
    Arquivo arquivo;

    arquivo.abrir_arq(); // abrir arquivo

    arquivo.tam_arq(); // tamanho do arquivo

    arquivo.ler_arq(); // ler arquivo

    arquivo.copia_arq(); // copia p/ conferir no HEX EDITOR

    arquivo.cont_bytes(); //contar a repeticao de Bytes

    return 0;
}

